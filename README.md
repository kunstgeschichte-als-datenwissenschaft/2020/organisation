# Kunstgeschichte als Datenwissenschaft

Im Seminar sollen computergestützt Annotationen zu Kunstwerken für deren historische Analyse herangezogen werden. Im ersten Fall wird ein professionell geschaffenes Klassifikationssystem zu einer großflächigen kunsthistorischen Einordnung genutzt, in den beiden anderen Fällen crowdgesourcte, also hauptsächlich von Laien eingegebene Daten.
Für Kunsthistoriker und Kunsthistorikerinnen ist die Zugangsweise ungewöhnlich. Beschäftigen sie sich gewöhnlich mit qualitativen Daten, geht es hier um quantitative – womit übrigens nicht gesagt werden soll, dass das eine mit dem anderen nichts zu tun hat. Ein Ziel des Seminars wird demnach sein, die Legitimität dieser letzten Aussage zu belegen. Um den nicht immer offensichtlichen Anteil der Kunstgeschichte am Seminar ein wenig zu klären, werden nach den Themenstellungen mögliche Fragenkomplexe benannt. 


## Thema 1

Mit dem niederländischen Klassifikationssystem [Iconclass](http://iconclass.org/) sind künstlerische Themen kodiert worden, die bis in die Moderne hinein Funktion und Bedeutung von Kunstwerken bestimmt haben. Historische Wandlungsprozesse lassen sich unter anderem auch anhand des Wandels von Themenprioritäten rekonstruieren. Über Kreuzung mit Entstehungsdaten sollen Konjunkturen von Bildthemen in größeren kunsthistorischen Datenbanken festgestellt werden. Im Idealfall sind auf diesem Wege großflächige Entwicklungen (z. B. im Hinblick auf Säkularisierung) identifizierbar, aber auch Unterschiede von Ikonographien in protestantischen bzw. katholischen Räumen.

* **Daten**
Ein *Dump* des [Iconclass](http://iconclass.org/)-Systems wird zur Verfügung gestellt. Weiterhin liegen über 500.000 Kunstwerke inklusive Metadaten vor, die mit Iconclass annotiert worden sind.
<p></p>

* **Mögliche Fragestellungen** 
  1. Die Geistesgeschichte spricht auf vielen Ebenen von einer Säkularisierung, Profanisierung und Entchristlichung seit der Aufklärung. Sind christliche Themen mit dem 18. Jahrhundert tatsächlich spärlicher vertreten? Können regionale Unterschiede festgestellt werden, d. h. unterscheiden sich bspw. protestantisch und katholisch geprägte Länder? Ist etwa im traditionell stark katholischen Spanien eine zurückhaltendere Tendenz zu beobachten?
<p></p>

* **Kunsthistorische Themen, die behandelt werden können**
  1. Was ist christliche Kunst?
  2. Was sind elementare Gegenstände der christlichen Ikonographie?
  3. Was ist Säkularisierung?
  4. In welchen Eigenheiten der Ikonographie zeigt sich die Konfessionalisierung?


## Thema 2

[ARTigo](http://www.artigo.org/) ist zweierlei: erstens eine Internetplattform, in der einem auf seine Qualifikation hin nicht näher befragten Publikum digitale Reproduktionen von Kunstwerken vorgelegt werden, um diese in einem spielerisch-kompetitiven Verfahren zu annotieren. Zweitens eine semantische Suchmaschine, die auf Basis der über die *Crowd* generierten Annotationen (*Tags*) großer Bildmengen Herr wird, ohne auf die Hilfe von Spezialisten setzen zu müssen. Seit 2007 wurden über dieses „Ökosystem“ 9,6 Millionen deutsch-, englisch- und französischsprachige Taggings für über 55.000 Kunstwerke gesammelt.

* **Daten**
Ein bereinigter *Dump* der [ARTigo](http://www.artigo.org/)-Datenbank mit 9.669.410 Taggings wird unter [Open Data LMU](https://data.ub.uni-muenchen.de/136/) zur Verfügung gestellt.
<p></p>

* **Mögliche Fragestellungen** 
  1. Weist die spezifische Zusammensetzung der *Tags* auf eine wahrscheinliche Datierung? Naheliegend ist dies bei stilgeschichtlichen Bezeichnungen, die häufig unter den *Tags* vorkommen, z. B. „barock“ oder „realistisch“. Sind neutralere Begriffe auch aufschlussreich? Etwa, dass das „Ross“ auf einen aristokratisch-barocken Zusammenhang verweist, während der „Gaul“ in einem realistischen Kunstwerk vorkommt? Oder die „Maschine“ wohl kaum (oder zumindest selten) vor dem 19. Jahrhundert?
  2. Interessant sind die falschen Angaben der *Crowd*, die den Künstler oder die Künstlerin betreffen. Beruhen diese Falschangaben auf künstlerischer Nähe? Ist etwa die Angabe „Liebermann“ bei einem Bild von Max Slevogt zwar falsch, angesichts der künstlerischen Ähnlichkeit der beiden aber nachvollziehbar?
<p></p>

* **Kunsthistorische Themen, die behandelt werden können**
  1. Was ist Ähnlichkeit in kunsthistorischer Perspektive?
  2. Was ist ein Stil?


## Thema 3

Film beeinflusst Film. So wurden Sequenzen und Motive z. B. aus Stanley Kubricks „2001: Odyssee im Weltraum“ (1968) vielfach aufgegriffen, von populären Filmen („Krieg der Sterne“, 1977) bis zu experimentellen Arthouse-Produktionen („Enter the Void“, 2009) und B-Movies („Der Rasenmähermann 2“, 1996). Zu untersuchen wäre: Welche Regisseure neigen wie bspw. Quentin Tarantino zur Selbstreferenzialität? Werden Filme bestimmter Genres besonders häufig zitiert? Sind zeitliche Schwankungen zu erkennen? Gibt es Konjunkturen, d. h. Filme, die in unregelmäßigen Abständen überdurchschnittlich häufig referenziert werden, oder solche, die konstant und permanent relevant sind?

* **Daten**
Ein *Dump* der [Internet Movie Database](https://www.imdb.com/) ([IMDb](https://www.imdb.com/)), der Spielfilme mit einer Laufzeit von über 39 Minuten enthält, wird zur Verfügung gestellt.
<p></p>

* **Mögliche Fragestellungen** 
  1. Alle zehn Jahre veröffentlicht die vom [British Film Institute](https://www.bfi.org.uk/) herausgegebene Zeitschrift [Sight & Sound](https://www.bfi.org.uk/news-opinion/sight-sound-magazine) eine Liste der *Besten Filme aller Zeiten* ([1952](https://web.archive.org/web/20141008052443/http://old.bfi.org.uk/sightandsound/polls/topten/history/1952.html), [1962](https://web.archive.org/web/20141008051815/http://old.bfi.org.uk/sightandsound/polls/topten/history/1962.html), [1972](https://web.archive.org/web/20141008052122/http://old.bfi.org.uk/sightandsound/polls/topten/history/1972.html), [1982](https://web.archive.org/web/20141008062954/http://old.bfi.org.uk/sightandsound/polls/topten/history/1982.html), [1992](https://web.archive.org/web/20141008052127/http://old.bfi.org.uk/sightandsound/polls/topten/history/1992.html), [2002](https://web.archive.org/web/20160813112813/http://old.bfi.org.uk/sightandsound/polls/topten/poll/critics-long.html), [2012](https://www.bfi.org.uk/greatest-films-all-time)). Schlägt sich eine Nennung auf dieser Liste auch quantitativ nieder? Wird bspw. „Citizen Kane“ (1941) signifikant häufiger zitiert als ein (repräsentativer) Film desselben Jahrgangs? Welche zeitlichen Effekte sind festzustellen? Nimmt der Einfluss früherer Filme per se sukzessive ab? Gibt es weitere relevante Indikatoren, z. B. die Sprache oder das Produktionsland eines Films?
  2. Selbstreferenzialität ist eine essenzielle Komponente des Slasherfilms (insbesondere der Neunzigerjahre), die häufig parodistisch genutzt wird („Scream“-Reihe, 1996, 1997, 2000, 2011). Gilt diese Annahme für alle Filme des Subgenres? Welche Ausreißer können identifiziert werden? Zeigt sich das Phänomen auch auf übergeordneter Ebene, d. h. allgemein für Filme des Genres Horrorfilm (z. B. im Splatter- und Gorefilm)? Zu welchen Zeitpunkten?
<p></p>

* **Filmwissenschaftliche Themen, die behandelt werden können**
  1. Was ist Ähnlichkeit in filmwissenschaftlicher Perspektive?
  2. Was sind (narrative und stilistische) Spezifika des Slasherfilms?
  

## Vorkenntnisse

Je nach Thema: Überblickskenntnisse über die frühneuzeitliche Kunst- und Kunsttheoriegeschichte, Kenntnisse der Kunstgeschichte des 19. Jahrhunderts, Kenntnisse der Filmgeschichte.


## Weitere Informationen

Es ist geplant, dass jedes Thema in einer Gruppe bearbeitet wird, die sich jeweils aus Studierenden der Informatik und Studierenden der Kunstgeschichte zusammensetzt. Ein kontinuierlicher Austausch zwischen den Studierenden der unterschiedlichen Disziplinen ist essenziell; d. h. informatische Inhalte sind in den jeweiligen Gruppen von dem Studierenden der Informatik zu vermitteln, kunsthistorische Konzepte von den Studierenden der Kunstgeschichte. Wir empfehlen den Studierenden der Kunstgeschichte zudem, zur selbstständigen Exploration der Datensätze auf das Online-Tool [Museum Analytics (MAX)](https://www.max.gwi.uni-muenchen.de/) zurückzugreifen.

Das Seminar wird vollständig online abgehalten: Die [einzelnen Sitzungen](https://git.dhvlab.org/kunstgeschichte-als-datenwissenschaft/organisation/-/blob/master/seminarplan.md) finden auf [Zoom](https://lmu-munich.zoom.us/) statt, für das Projektmanagement wird [GitLab](https://git.dhvlab.org/kunstgeschichte-als-datenwissenschaft) und für die -kommunikation [Mattermost](https://teams.dhvlab.org/kg-als-dw-ss-20/) eingesetzt. Zur Vorbereitung sei auf die jeweiligen Tutorials verwiesen: [Zoom](https://support.zoom.us/hc/de/categories/200101697), [GitLab](https://docs.gitlab.com/), [Mattermost](https://docs.mattermost.com/guides/user.html). Alle Teilnehmer und Teilnehmerinnen haben sich im [DHVLab](https://dhvlab.gwi.uni-muenchen.de/mgmt/labuser/signup) anzumelden (*Lab*: „KG als DW SS 20“).


## Kontakt

Hubertus Kohle ([Website](https://www.kunstgeschichte.uni-muenchen.de/personen/professoren_innen/kohle/index.html)), Stefanie Schneider ([Website](https://www.kunstgeschichte.uni-muenchen.de/personen/wiss_ma/schneider/index.html)), François Bry ([Website](https://www.pms.ifi.lmu.de/mitarbeiter/derzeitige/francois-bry/index.html)), Martin Bogner ([Website](https://www.pms.ifi.lmu.de/mitarbeiter/derzeitige/martin-bogner/index.html)).


## Literatur

* [Bry, François und Christoph Wieser (2012): „Squaring and Scripting the ESP Game: Trimming a GWAP to Deep Semantics“, in: *Proceedings of the International Conference on Serious Games Development and Applications*.](http://www.en.pms.ifi.lmu.de/publications/PMS-FB/PMS-FB-2012-10/PMS-FB-2012-10-paper.pdf)

* [Canet, Fernando, Miguel Á. Valero und Lluís Codina (2016): „Quantitative Approaches for Evaluating the Influence of Films Using the IMDb Database“, in: *Communication & Society* 29.2.](https://dadun.unav.edu/bitstream/10171/41904/1/09.pdf)

* Dika, Vera (1990): „Games of Terror: Halloween, Friday the 13th, and the Films of the Stalker Cycle“, Rutherford, N. J.: Fairleigh Dickinson University Press.

* [Gallos, Lazaros K., Fabricio Q. Potiguar, José S. Andrade Jr und Hernan A. Makse (2013): „IMDB Network Revisited: Unveiling Fractal and Modular Properties from a Typical Small-World Network“, in: *PLoS ONE* 8.6](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0066443)

* [Glinka, Katrin, Christopher Pietsch und Marian Dörk (2017): „Past Visions and Reconciling Views. Visualizing Time, Texture and Themes in Cultural Collections“, in: *Digital Humanities Quarterly* 11.2.](http://www.digitalhumanities.org/dhq/vol/11/2/000290/000290.html)

* Jannidis, Fotis, Hubertus Kohle und Malte Rehbein (Hrsg., 2017): „Digital Humanities. Eine Einführung“, Stuttgart: J. B. Metzler.

* Liebenwein, Renate (1977): „Säkularisierung und Sakralisierung: Studien zum Bedeutungswandel christlicher Bildformen in der Kunst des 19. Jahrhunderts“, Dissertation.

* Manchel, Frank (1990): „Film Study: An Analytical Bibliography“, Rutherford, N. J.: Fairleigh Dickinson University Press.

* [Schneider, Stefanie und Hubertus Kohle (2017): „The Computer as Filter Machine: A Clustering Approach to Categorize Artworks Based on a Social Tagging Network“, in: *Artl\@s Bulletin* 6.2.](https://docs.lib.purdue.edu/cgi/viewcontent.cgi?article=1141&context=artlas)

* Suckale, Robert (2005): „Geschichte der Kunst in Deutschland“, Köln: Dumont.

* van de Waal, Henri (1973–1985): „Iconclass: An Iconographic Classification System. Completed and Edited by L. D. Couprie with R. H. Fuchs“, Amsterdam: North-Holland Publishing Company.

* van Straten, Roelof (1994): „Iconography, Indexing, Iconclass. A Handbook“, Leiden: Foleor Publishers.

* [Wieser, Christoph, François Bry, Alexandre Bérard und Richard Lagrange (2017): „ARTigo: Building an Artwork Search Engine With Games and Higher-Order Latent Semantic Analysis“, in: *Workshop on Human Computation and Machine Learning in Games*.](https://www.en.pms.ifi.lmu.de/publications/PMS-FB/PMS-FB-2013-3/PMS-FB-2013-3-paper.pdf)

* [Windhager, Florian, Paolo Federico, Eva Mayr, Günther Schreder und Michael Smuc (2016): „A Review of Information Visualization Approaches and Interfaces to Digital Cultural Heritage Collections“.](https://publik.tuwien.ac.at/files/publik_257936.pdf)
