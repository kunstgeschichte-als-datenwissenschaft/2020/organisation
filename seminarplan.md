 1. *22.04*: Einführung (kurze Vorstellung der Seminarleiter und -teilnehmer), Erläuterung der unterschiedlichen Datensätze, Vergabe der Projektthemen, Leistungsnachweise.
 2. *29.04*: Digitale Kunstgeschichte.
 3. *06.05*: Kurzpräsentationen (Studierende der Kunstgeschichte): Forschungsfragen und Forschungsstand aus theoretischer (d. h. kunsthistorischer oder filmwissenschaftlicher) Perspektive.
 4. *13.05*: Kurzpräsentationen (Studierende der Informatik): Erste Schritte zur Bereinigung des jeweiligen Datensatzes, Deskription, Auswahl informatischer und statistischer Methoden.
 5. *20.05*: Abgabe und Diskussion des Arbeitspapiers (Forschungsfrage und -stand, Aufgabenverteilung, Zeitplan).
 6. *27.05*: Puffer.
 7. *03.06*: Freie Sitzung zur individuellen Betreuung der Gruppen.
 8. *10.06*: Zwischenpräsentationen (1, nicht öffentlich): 10 bis 15 Minuten Vortrag plus Diskussion (insgesamt maximal 45 Minuten).
 9. *17.06*: Zwischenpräsentationen (2, nicht öffentlich): 10 bis 15 Minuten Vortrag plus Diskussion (insgesamt maximal 45 Minuten).
10. *24.06*: Freie Sitzung zur individuellen Betreuung der Gruppen.
11. *01.07*: Freie Sitzung zur individuellen Betreuung der Gruppen.
12. *08.07*: Endpräsentationen (1, öffentlich): 20 bis 25 Minuten Vortrag plus Diskussion (insgesamt maximal 45 Minuten).
13. *15.07*: Endpräsentationen (2, öffentlich): 20 bis 25 Minuten Vortrag plus Diskussion (insgesamt maximal 45 Minuten).
14. *22.07*: Abschlussdiskussion, Fazit, Feedback und Evaluation.
